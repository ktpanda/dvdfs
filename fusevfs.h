// -*- c++ -*-
/*
  DVDfs - mount a DVD using libdvdread (and libdvdcss, if installed)
  Copyright (C) 2009  Katie Rust (ktpanda)

  Based on fusexmp_fh template

  This program can be distributed under the terms of the GNU GPL.
  See the file COPYING.
*/

#ifndef _FUSEVFS_H
#define _FUSEVFS_H

#define VFS_BLOCK_SIZE 2048

#include <fuse.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>

#include <string>
#include <map>

#include "util.h"

class FuseFile : public Refcount {
public:
    struct stat file_stat;
    int opencount;


    FuseFile();
    ~FuseFile() { if (opencount) close(); }

    virtual void open() { }
    virtual void close() { }

    virtual int stat(struct stat* st) {
        memcpy(st, &file_stat, sizeof(struct stat));
        return 0;
    }

    virtual int readdir(void *buf, fuse_fill_dir_t filler) {
        return -ENOTDIR;
    }

    virtual int findchild(const string& path, string::size_type pos, sptr<FuseFile>& file) {
        file = NULL;
        return -ENOTDIR;
    }

    sptr<FuseFile> findchild(const string& path) {
        sptr<FuseFile> f;
        if (findchild(path, 0, f))
            return NULL;
        return f;
    }

    virtual int read(char* buffer, size_t size, off_t offset) {
        return -EIO;
    }
    virtual int write(const char* buffer, size_t size, off_t offset) {
        return -EIO;
    }

    void touch();
};

class FuseDirectory : public FuseFile {
public:
    typedef map< string, sptr<FuseFile> > childmap;
    childmap children;

    FuseDirectory();

    virtual int readdir(void *buf, fuse_fill_dir_t filler);

    int findchild(const string& path, string::size_type spos, sptr<FuseFile>& file);

    void addfile(const string& name, FuseFile* f);
    void delfile(const string& name);
};


class StaticFile : public FuseFile {
public:
    char* data;
    size_t size;

    StaticFile(char* data=NULL, size_t size=0);
    ~StaticFile() { delete[] data; }

    virtual int read(char* buffer, size_t size, off_t offset);
    void update_stat() {
        file_stat.st_size = size;
        file_stat.st_blocks = (size + VFS_BLOCK_SIZE - 1) / VFS_BLOCK_SIZE;
    }
};


extern sptr<FuseDirectory> vfs_root;
extern struct fuse_operations vfs_oper;

#endif
