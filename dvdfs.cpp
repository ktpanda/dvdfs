/*
  DVDfs - mount a DVD using libdvdread (and libdvdcss, if installed)
  Copyright (C) 2009  Katie Rust (ktpanda)

  Based on fusexmp_fh template

  This program can be distributed under the terms of the GNU GPL.
  See the file COPYING.
*/

using namespace std;
#include <fuse.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <ctype.h>
#include <dvdread/dvd_reader.h>

#include <string>
#include <vector>
#include <map>

#include "fusevfs.h"

#define GIG_BLOCKS ((1024*1024*1024) / DVD_VIDEO_LB_LEN)
#define MAX_TITLES 100

static char action_inst[] =
    "Write to this file to perform actions, e.g.\n"
    "\n"
    "echo close > .action\n"
    "\n"
    "The following actions are defined:\n"
    "\n"
    "close        Close the dvd and free info cache\n"
    "open         Re-open the device and load info files\n";


static dvd_reader_t* dvdreader;

class VOBFile : public FuseFile {
public:
    static long file_stamp;
    long stamp;
    int title;
    dvd_read_domain_t domain;
    int base;
    int size;

    dvd_file_t* dvdfile;

    VOBFile(int title, dvd_read_domain_t domain, int blocksize,
            int base);

    inline int read_blocks(int begin, int nblocks, char* buf);
    virtual void open();
    virtual void close();
    virtual int read(char* buffer, size_t size, off_t offset);
};

class ActionFile : public StaticFile {
public:
    ActionFile() : StaticFile(action_inst, sizeof(action_inst) - 1) {
        file_stat.st_mode = S_IFREG | 0666;
        makestatic();
    }
    virtual int write(const char*, size_t, off_t);
};

VOBFile::VOBFile(int title, dvd_read_domain_t domain, int blocksize,
                       int base) : title(title), domain(domain), base(base),
                                   size(blocksize)
{
    file_stat.st_mode = S_IFREG | 0444;
    if (size > GIG_BLOCKS) size = GIG_BLOCKS;
    file_stat.st_blocks = size;
    file_stat.st_size = size * VFS_BLOCK_SIZE;

    stamp = file_stamp;

}

void VOBFile::open() {
    if (stamp != file_stamp)
        return;
    dvdfile = DVDOpenFile(dvdreader, title, domain);
}

void VOBFile::close() {
    if (dvdfile)
        DVDCloseFile(dvdfile);
    dvdfile = NULL;
}

struct dvdfs_opts {
    char* device;
    int includebups;
    int dynamic;
};

static struct dvdfs_opts dvdfs_opts;

static int dvd_num_titles = 0;
long VOBFile::file_stamp = 0;


/*
 OK, this is the tricky part. There are two separate functions for
 reading, one for info and one for vobs. The one for info reads an
 arbitrary number of bytes, but the one for vobs reads only
 blocks. Some magic is needed to read arbitrary ranges from vobs.

 Also, it seems that DVDReadBytes can fail (returning 0, not -1) under
 certain circumstances. To minimize the chance of this happening, I
 read the info files on startup and save the contents.
 */

inline int VOBFile::read_blocks(int begin, int nblocks, char* buf) {
    return DVDReadBlocks(dvdfile, begin + base, nblocks, (unsigned char*)buf);
}

int VOBFile::read(char* buf, size_t size, off_t offset) {
    char temp_block[DVD_VIDEO_LB_LEN];
    int bytes_read, nblocks;
    int begin_blockpos, end_blockpos;
    int bytes_at_beginning, bytes_at_end;
    int res;

    if (!dvdfile || stamp != file_stamp) {
        close();
        return 0;
    }

    bytes_read = 0;
    begin_blockpos = offset / DVD_VIDEO_LB_LEN;
    end_blockpos = (offset + size) / DVD_VIDEO_LB_LEN;
    bytes_at_beginning =  offset - (begin_blockpos * DVD_VIDEO_LB_LEN);
    bytes_at_end = (offset + size) - (end_blockpos * DVD_VIDEO_LB_LEN);

    if (begin_blockpos == end_blockpos) {
        res = read_blocks(begin_blockpos, 1, temp_block);
        if (res == -1)
            return -EIO;
        if (res == 0)
            return 0;

        memcpy(buf, temp_block + (DVD_VIDEO_LB_LEN - bytes_at_beginning), size);
    } else {
        if (bytes_at_beginning)
            begin_blockpos++;

        /* Read a partial chunk at the beginning of the buffer */
        if (bytes_at_beginning) {
            res = read_blocks(begin_blockpos - 1, 1, temp_block);
            if (res == -1)
                return -EIO;
            if (res == 0)
                return 0;

            memcpy(buf, temp_block + bytes_at_beginning, DVD_VIDEO_LB_LEN - bytes_at_beginning);
            bytes_read += bytes_at_beginning;
        }
        /* Read a the complete blocks from the middle */
        if (begin_blockpos != end_blockpos) {
            nblocks = end_blockpos - begin_blockpos;
            res = read_blocks(begin_blockpos, nblocks, &buf[bytes_read]);
            if (res == -1)
                return -EIO;
            bytes_read += (DVD_VIDEO_LB_LEN * res);
            if (res < nblocks)
                return bytes_read;

        }
        /* Read a partial chunk at the end of the buffer */
        if (bytes_at_end) {
            res = read_blocks(end_blockpos, 1, temp_block);
            if (res == -1)
                return -EIO;
            if (res == 0)
                return bytes_read;

            memcpy(&buf[bytes_read], temp_block, bytes_at_end);
            bytes_read += bytes_at_beginning;
        }

        return bytes_read;
    }
    if (res == -1)
	return -EIO;
    return res;
}

/*
  The DVD video file structure looks like this:

  /
  /VIDEO_TS
  /VIDEO_TS/VIDEO_TS.IFO
  /VIDEO_TS/VIDEO_TS.BUP
  /VIDEO_TS/VIDEO_TS.VOB

  /VIDEO_TS/VTS_01_0.IFO
  /VIDEO_TS/VTS_01_0.BUP
  /VIDEO_TS/VTS_01_1.VOB
  ...
  /VIDEO_TS/VTS_01_n.VOB

  ...

  /VIDEO_TS/VTS_nn_0.IFO
  /VIDEO_TS/VTS_nn_0.BUP
  /VIDEO_TS/VTS_nn_1.VOB
  ...
  /VIDEO_TS/VTS_nn_n.VOB


  The dvdread library does not present us with a filesystem, so we
  need to convert paths to title, domain, and base offset. Also, it
  presents title VOBs as one large file, so we need to split the title
  VOBs into 1 gigabyte chunks.

 */

#define DVDFS_OPT(t, p, v) { t, offsetof(struct dvdfs_opts, p), v }

static struct fuse_opt dvdfs_opts_table[] = {
    DVDFS_OPT("device=%s", device, 0),
    DVDFS_OPT("includebups", includebups, 1),
    DVDFS_OPT("dynamic", dynamic, 1),

    { NULL }
};

static int get_size(int title, dvd_read_domain_t domain) {
    dvd_file_t* f = DVDOpenFile(dvdreader, title, domain);
    if (!f) return -1;
    int ret = DVDFileSize(f);
    DVDCloseFile(f);
    return ret;
}

static int read_titles(void) {
    int title;
    bool exists;
    int size, rsize;
    dvd_file_t* f;
    char tmp_filename[20];


    sptr<FuseDirectory> video_ts = new FuseDirectory();
    vfs_root->addfile("VIDEO_TS", video_ts);

    printf("Scanning DVD ...\n");
    for (title = 0; title < MAX_TITLES; title++) {
	exists = false;

        if ((size = get_size(title, DVD_READ_MENU_VOBS)) != -1) {
            sptr<VOBFile> nfile = new VOBFile(title, DVD_READ_MENU_VOBS, size, 0);
            if (title == 0) {
                video_ts->addfile("VIDEO_TS.VOB", nfile);
            } else {
                snprintf(tmp_filename, sizeof(tmp_filename), "VTS_%02d_0.VOB", title);
                video_ts->addfile(tmp_filename, nfile);
            }
            exists = true;
        }
        if (title != 0 && (size = get_size(title, DVD_READ_TITLE_VOBS)) != -1) {
            int i = 1;
            int base = 0;
            while (size > 0) {
                sptr<VOBFile> nfile = new VOBFile(title, DVD_READ_TITLE_VOBS, size, base);
                snprintf(tmp_filename, sizeof(tmp_filename), "VTS_%02d_%d.VOB", title, i);
                video_ts->addfile(tmp_filename, nfile);

                i++;
                base += GIG_BLOCKS;
                size -= GIG_BLOCKS;
            }
            exists = true;
        }

	f = DVDOpenFile(dvdreader, title, DVD_READ_INFO_FILE);
	if (f) {
	    size = DVDFileSize(f) * VFS_BLOCK_SIZE;
            sptr<StaticFile> nfile = new StaticFile(NULL, size);
	    rsize = DVDReadBytes(f, nfile->data, size);
	    DVDCloseFile(f);

            exists = true;

            if (title == 0) {
                video_ts->addfile("VIDEO_TS.IFO", nfile);
                if (dvdfs_opts.includebups)
                    video_ts->addfile("VIDEO_TS.BUP", nfile);
            } else {
                snprintf(tmp_filename, sizeof(tmp_filename), "VTS_%02d_0.IFO", title);
                video_ts->addfile(tmp_filename, nfile);
                if (dvdfs_opts.includebups) {
                    snprintf(tmp_filename, sizeof(tmp_filename), "VTS_%02d_0.BUP", title);
                    video_ts->addfile(tmp_filename, nfile);
                }
            }
	}

	if (!exists) break;
    }
    printf("Scan complete!\n");
    return title;
}


static void close_dvd() {
    vfs_root->delfile("VIDEO_TS");
    if (dvdreader) {
        DVDClose(dvdreader);
        dvdreader = NULL;
    }
    VOBFile::file_stamp++;
}

static int open_dvd() {
    close_dvd();

    dvdreader = DVDOpen(dvdfs_opts.device);
    if (!dvdreader) {
	fprintf(stderr, "Could not open %s\n", dvdfs_opts.device);
	return -1;
    }

    dvd_num_titles = read_titles();
    return 0;
}

int ActionFile::write(const char* buf, size_t size, off_t offset) {
    if (offset == 0) {
        string action(buf, size);
        string::size_type nl_pos = action.find('\n');
        if (nl_pos != string::npos)
            action.erase(nl_pos);
        if (action == "open") {
            open_dvd();
        } else if (action == "close") {
            close_dvd();
        } else {
            return -EINVAL;
        }
        return size;
    }
    return -EINVAL;
}

int main(int argc, char *argv[]) {
    struct fuse_args args = FUSE_ARGS_INIT(argc, argv);

    vfs_root = new FuseDirectory();

    dvdfs_opts.device = "/dev/dvd";
    dvdfs_opts.includebups = 0;
    dvdfs_opts.dynamic = 0;

    if (fuse_opt_parse(&args, &dvdfs_opts, dvdfs_opts_table, NULL) == -1)
        return 1;

    if (dvdfs_opts.dynamic)
        vfs_root->addfile(".action", new ActionFile());

    open_dvd();
    return fuse_main(args.argc, args.argv, &vfs_oper);
}
