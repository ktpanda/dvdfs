/*
  DVDfs - mount a DVD using libdvdread (and libdvdcss, if installed)
  Copyright (C) 2009  Katie Rust (ktpanda)

  Based on fusexmp_fh template

  This program can be distributed under the terms of the GNU GPL.
  See the file COPYING.
*/

#include <time.h>
#include "fusevfs.h"

sptr<FuseDirectory> vfs_root;

FuseFile::FuseFile() : opencount(0) {

    file_stat.st_dev = 0;
    file_stat.st_ino = 0;
    file_stat.st_mode = 0;
    file_stat.st_nlink = 1;
    file_stat.st_uid = 0;
    file_stat.st_gid = 0;
    file_stat.st_rdev = 0;
    file_stat.st_blksize = VFS_BLOCK_SIZE;

    file_stat.st_blocks = 0;
    file_stat.st_size = 0;
    touch();
}

void FuseFile::touch() {
    time_t ctime = time(NULL);
    file_stat.st_atime = ctime;
    file_stat.st_mtime = ctime;
    file_stat.st_ctime = ctime;
}
StaticFile::StaticFile(char* _data, size_t _size) : data(_data), size(_size) {
    if (size && !data) {
        data = new char[size];
    }
    file_stat.st_mode = S_IFREG | 0444;
    update_stat();
}

int StaticFile::read(char* buffer, size_t rsize, off_t offset) {
    size_t btr;
    if (data && (size_t)offset < size) {
        btr = rsize;
        if (offset + btr > size) btr = size - offset;
        memcpy(buffer, data + offset, btr);
        return btr;
    }
    return 0;
}

FuseDirectory::FuseDirectory() {
    file_stat.st_mode = S_IFDIR | 0555;
}

int FuseDirectory::readdir(void *buf, fuse_fill_dir_t filler)
{

    filler(buf, ".", NULL, 0);
    filler(buf, "..", NULL, 0);

    for (childmap::iterator itr = children.begin();
         itr != children.end();
         itr++) {
        filler(buf, itr->first.c_str(), &itr->second->file_stat, 0);
    }
    return 0;
}

int FuseDirectory::findchild(const string& path, string::size_type spos, sptr<FuseFile>& file) {

    while (spos < path.size() && path[spos] == '/') spos++;

    string::size_type slash_pos = path.find('/', spos);
    string name(path, spos, slash_pos == string::npos ? string::npos : slash_pos - spos);

    if (name.empty()) {
        file = this;
        return 0;
    }

    childmap::iterator itr = children.find(name);
    if (itr == children.end()) {
        return -ENOENT;
    }

    if (slash_pos == string::npos) {
        file = itr->second;
        return 0;
    }
    return itr->second->findchild(path, slash_pos + 1, file);
}

void FuseDirectory::addfile(const string& name, FuseFile* f) {
    children[name] = f;
    touch();
}
void FuseDirectory::delfile(const string& name) {
    children.erase(name);
    touch();
}

static inline FuseFile* get_vfs_file(struct fuse_file_info *fi) {
    return reinterpret_cast<FuseFile*>(fi->fh);;
}

static inline int get_vfs_file(const char* path, sptr<FuseFile>& f) {
    if (!vfs_root)
        return -ENOENT;
    return vfs_root->findchild(path, 0, f);
}


static int vfs_erofs(const char *path, ...) {
    return -EROFS;
}
static int vfs_ignore(const char *path, ...) {
    return 0;
}


static int vfs_getattr(const char *path, struct stat *st) {
    int res;
    sptr<FuseFile> file;
    if ((res = get_vfs_file(path, file)))
        return res;

    file->stat(st);
    return 0;
}

static int vfs_fgetattr(const char *path, struct stat *st,
                        struct fuse_file_info *fi) {
    FuseFile* f = get_vfs_file(fi);

    (void) path;

    f->stat(st);
    return 0;
}

static int vfs_access(const char *path, int mask) {
    sptr<FuseFile> file;
    return get_vfs_file(path, file);
}

static int vfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                       off_t offset, struct fuse_file_info *fi) {

    int res;
    sptr<FuseFile> file;
    if ((res = get_vfs_file(path, file)))
        return res;

    return file->readdir(buf, filler);
}

static int vfs_open(const char *path, struct fuse_file_info *fi) {
    int res;
    sptr<FuseFile> file;
    if ((res = get_vfs_file(path, file)))
        return res;

    if (S_ISDIR(file->file_stat.st_mode)) {
        return -EISDIR;
    }

    if (!file->opencount++) {
        file->open();
    }

    fi->fh = reinterpret_cast<unsigned long>(&*file);
    file->ref();

    return 0;

}


static int vfs_read(const char *path, char *buf, size_t size, off_t offset,
                    struct fuse_file_info *fi) {
    FuseFile* f = get_vfs_file(fi);
    (void) path;

    return f->read(buf, size, offset);
}

static int vfs_write(const char *path, const char *buf, size_t size, off_t offset,
                    struct fuse_file_info *fi) {
    FuseFile* f = get_vfs_file(fi);
    (void) path;

    return f->write(buf, size, offset);
}

static int vfs_release(const char *path, struct fuse_file_info *fi) {
    FuseFile* f = get_vfs_file(fi);
    (void) path;

    if (!--f->opencount) {
        f->close();
    }
    f->deref();
    return 0;
}

static int vfs_fsync(const char *path, int isdatasync,
                     struct fuse_file_info *fi) {
    return 0;
}

struct fuse_operations vfs_oper = {
    vfs_getattr, /* getattr */
    NULL, /* readlink */
    NULL, /* getdir */
    (int(*)(const char*, mode_t, dev_t)) vfs_erofs, /* mknod */
    (int(*)(const char*, mode_t)) vfs_erofs, /* mkdir */
    (int(*)(const char*)) vfs_erofs, /* unlink */
    (int(*)(const char*)) vfs_erofs, /* rmdir */
    (int(*)(const char*, const char*)) vfs_erofs, /* symlink */
    (int(*)(const char*, const char*)) vfs_erofs, /* rename */
    (int(*)(const char*, const char*)) vfs_erofs, /* link */
    (int(*)(const char*, mode_t)) vfs_erofs, /* chmod */
    (int(*)(const char*, uid_t, gid_t)) vfs_erofs, /* chown */
    (int(*)(const char*, off_t)) vfs_ignore, /* truncate */
    (int(*)(const char*, struct utimbuf*)) vfs_erofs, /* utime */
    vfs_open, /* open */
    vfs_read, /* read */
    vfs_write, /* write */
    NULL, /* statfs */
    NULL, /* flush */
    vfs_release, /* release */
    vfs_fsync, /* fsync */
    NULL, /* setxattr */
    NULL, /* getxattr */
    NULL, /* listxattr */
    NULL, /* removexattr */
    NULL, /* opendir */
    vfs_readdir, /* readdir */
    NULL, /* releasedir */
    NULL, /* fsyncdir */
    NULL, /* init */
    NULL, /* destroy */
    vfs_access, /* access */
    (int(*)(const char*, mode_t, struct fuse_file_info *)) vfs_erofs, /* create */
    (int(*)(const char*, off_t, struct fuse_file_info*)) vfs_erofs, /* ftruncate */
    vfs_fgetattr, /* fgetattr */
};
